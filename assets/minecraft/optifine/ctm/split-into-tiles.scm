(script-fu-register
    "script-fu-split-into-tiles"
    "Split image into tiles and export them"
    "Takes a large image and exports tiles \
    from it at the chosen resolution. Made for\
    the \"repeat\" ctm method"
    "Robert Rapier <xtrumpeterx@yahoo.com>"
    "MIT License"
    "October 20, 2019"
    ""

    SF-IMAGE    "Image"  0 ;inImage
    SF-DRAWABLE "Drawable"  0 ;inDrawable
    SF-DIRNAME "Output Folder" "%USERPROFILE%\\Pictures\\GIMP\\Output" ;inOutDir
    SF-OPTION "Type" '("Texture" "Specular Map" "Normal Map") ;inType
    SF-ADJUSTMENT "First Output Tile #" '(0 0 9999 1 1 0 1) ;inFirst
    SF-ADJUSTMENT "Tile resolution" '(256 0 999999 1 1 0 1) ;inRes
)
(script-fu-menu-register "script-fu-split-into-tiles" "<Image>/Image")
(define (script-fu-split-into-tiles inImage inDrawable inOutDir inType inFirst inRes)
    (let*
        (
            (imageWidth (car (gimp-drawable-width inDrawable )))
            (imageHeight (car (gimp-drawable-height inDrawable )))
            (tileWidth (inexact->exact (ceiling (/ imageWidth inRes))))
            (tileHeight (inexact->exact (ceiling (/ imageHeight inRes))))
            (workLayer 0)
            (fileEnding (if (equal? inType 0)(string-append ".png")
                            (if (equal? inType 1)(string-append "_s.png")
                            (if (equal? inType 2)(string-append "_n.png")))))
        )
        (gimp-image-resize inImage inRes inRes 0 0)
        (do ((y 1 (set! y (+ y 1)))(i 0))
            ((> y tileHeight))
            (set! workLayer (car (gimp-layer-new-from-visible inImage inImage "workLayer")))
            (gimp-image-insert-layer inImage workLayer 0 0)
            (file-png-save-defaults 1 inImage workLayer (string-append inOutDir "\\" (number->string i) fileEnding) inOutDir)
            (gimp-image-remove-layer inImage workLayer)
            (set! i (+ i 1))
            (do ((x 1 (set! x (+ x 1))))
                ((>= x tileWidth))
                (gimp-image-resize inImage inRes inRes (- inRes) 0)
                (set! workLayer (car (gimp-layer-new-from-visible inImage inImage "workLayer")))
                (gimp-image-insert-layer inImage workLayer 0 0)
                (file-png-save-defaults 1 inImage workLayer (string-append inOutDir "\\" (number->string i) fileEnding) inOutDir)
                (gimp-image-remove-layer inImage workLayer)
                (set! i (+ i 1))
            )
            (gimp-image-resize inImage inRes inRes (- imageWidth inRes) (- inRes))
        )
        (gimp-image-resize inImage imageWidth imageHeight 0 imageHeight)
    )
) 