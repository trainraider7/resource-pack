(script-fu-register
    "script-fu-make-overlay"
    "Make Overlay"
    "Takes an input image and combines \
    it with a set of masks, creating a \
    copy of the input with each mask."
    "Robert Rapier <xtrumpeterx@yahoo.com>"
    "MIT License"
    "October 10, 2019"
    ""

    SF-FILENAME "Base Image" "" ;inBaseFile
    SF-DIRNAME "Map Folder" "%USERPROFILE%\\Pictures\\GIMP\\Maps" ;inMapFolder
    SF-DIRNAME "Output Folder" "%USERPROFILE%\\Pictures\\GIMP\\Output" ;inOutputFolder
    SF-OPTION "Type" '("Texture" "Specular Map" "Normal Map") ;inType
    SF-TOGGLE "Is specular alpha emissive?" TRUE; inFillAlpha
    SF-ADJUSTMENT "First input Map #" '(0 0 9999 1 1 0 1) ;inFirst
    SF-ADJUSTMENT "First Output Map #" '(0 0 9999 1 1 0 1) ;inFirst
    SF-ADJUSTMENT "Last Output Map #" '(16 0 9999 1 1 0 1) ;inLast
    SF-TOGGLE "Darken Edges" FALSE;inDarken
    SF-ADJUSTMENT "Darken Intensity" '(50 0 100 1 1 0 1) ;inIntense
    SF-ADJUSTMENT "Darken Radius" '(1 1 9999 1 1 0 1) ;inRad
)
(script-fu-menu-register "script-fu-make-overlay" "<Image>/Image")
(define (script-fu-make-overlay inBaseFile inMapFolder inOutputFolder inType inFillAlpha inStart inFirst inLast inDarken inIntense inRad)
    (define (basic-blur image drawable radius)
        (do ((i 0 (set! i (+ i 1))))
            ((>= i radius))
            (plug-in-blur 0 image drawable)

        )
    )
    (do ((i inFirst (set! i (+ i 1)))(j inStart (set! j (+ j 1))))
        ((> i inLast))
        (let* 
            (
                ;load texture
                (image (car (file-png-load 1 inBaseFile inBaseFile)))
                (res (car (gimp-image-width image)))
                (base (car (gimp-image-get-active-layer image)))
                (mask -1)
                (inputFile (string-append inMapFolder "\\" (number->string j) ".png"))
                (fileEnding (if (equal? inType 0)(string-append ".png")
                            (if (equal? inType 1)(string-append "_s.png")
                            (if (equal? inType 2)(string-append "_n.png")))))
                (outputFile (string-append inOutputFolder "\\" (number->string i) fileEnding))
                ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                ;The radius for darkening edges is effected by two seperate blurring steps. The radius
                    ;must be split up this way to create odd numbered radii.
                (quotRad (quotient inRad 2))
                (modRad  (modulo   inRad 2))
                ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                (map -1)
                (darkMap -1)
                (darkMask -1)
                (black -1)
                (normal -1)
                (normalMap -1)
                (normalMask -1)
                (specular -1)
                (specularMap -1)
                (specularMask -1)
            )
            ;Convert non-rgb images to rgb
            (if (equal? (car (gimp-drawable-is-rgb base)) 0) (gimp-image-convert-rgb image))
            ;add alpha channel to base layer
            (gimp-layer-add-alpha base)
            ;map, the layer made from the imported map file
            (set! map (car (gimp-file-load-layer 1 image inputFile)))
            ;add map layer to image
            (gimp-image-insert-layer image map 0 0)
            (cond ((equal? inDarken TRUE)
                (set! black (car(gimp-layer-new image res res 1 "black-layer" inIntense 28)))
                (gimp-image-insert-layer image black 0 0)
                (gimp-drawable-fill black 2)
                (gimp-drawable-invert black TRUE)
                (set! darkMap (car(gimp-layer-new image res res 1 "dark-map" 100 28)))
                (gimp-image-insert-layer image darkMap 0 0)
                (gimp-edit-copy map)
                (gimp-floating-sel-anchor (car (gimp-edit-paste darkMap TRUE)))
                (gimp-drawable-invert darkMap TRUE)
                (basic-blur image darkMap quotRad)
                (gimp-drawable-threshold darkMap 0 0.0038 1)
                (basic-blur image darkMap (+ quotRad modRad))
                (gimp-edit-cut darkMap)
                (set! darkMask (car (gimp-layer-create-mask black 0)))
                (gimp-layer-add-mask black darkMask)
                (gimp-floating-sel-anchor (car (gimp-edit-paste darkMask TRUE)))
                (gimp-layer-remove-mask black 0)
                (gimp-image-lower-item-to-bottom image black)
                (gimp-image-lower-item-to-bottom image base)
                (set! base (car(gimp-image-merge-down image black 1)))
            ))
            ;mask is id of mask layer
            (set! mask (car (gimp-layer-create-mask base 0)))
            ;bind new mask to base
            (gimp-layer-add-mask base mask)
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ;If this is a normal map, fill in the extra space with the correct normal color
            (cond ((equal? inType 2)
                ;create the normal background layer
                (set! normal (car (gimp-layer-new image res res 1 "normal-background" 100 28)))
                (gimp-image-insert-layer image normal 0 0)
                (gimp-context-set-foreground '(127 127 255))
                (gimp-drawable-edit-fill normal 0)
                ;create the mask for the background layer
                (set! normalMap (car (gimp-layer-new-from-drawable map image)))
                (gimp-image-insert-layer image normalMap 0 0)
                (gimp-drawable-invert normalMap TRUE)
                (set! normalMask (car (gimp-layer-create-mask normal 0)))
                ;apply mask
                (gimp-layer-add-mask normal normalMask)
                (gimp-edit-cut normalMap)
                (gimp-floating-sel-anchor (car (gimp-edit-paste normalMask TRUE)))
                (gimp-layer-remove-mask normal 0)
            ))
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ;if this is a specular map with emissive alpha, fill empty space with black
            (cond ((and (equal? inType 1) (equal? inFillAlpha TRUE))
                ;create the background layer
                (set! specular (car (gimp-layer-new image res res 1 "specular-background" 100 28)))
                (gimp-image-insert-layer image specular 0 0)
                (gimp-context-set-foreground '(0 0 0))
                (gimp-drawable-edit-fill specular 0)
                ;create the mask for the background layer
                (set! specularMap (car (gimp-layer-new-from-drawable map image)))
                (gimp-image-insert-layer image specularMap 0 0)
                (gimp-drawable-invert specularMap TRUE)
                (set! specularMask (car (gimp-layer-create-mask specular 0)))
                ;apply mask
                (gimp-layer-add-mask specular specularMask)
                (gimp-edit-cut specularMap)
                (gimp-floating-sel-anchor (car (gimp-edit-paste specularMask TRUE)))
                (gimp-layer-remove-mask specular 0)
            ))
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ;cut the map layer
            (gimp-edit-cut map)
            ;paste into mask layer
            (gimp-floating-sel-anchor (car (gimp-edit-paste mask TRUE)))
            ;apply mask to base
            (gimp-layer-remove-mask base 0)
            (if (equal? inType 2)(set! base (car (gimp-image-merge-down image normal 1))))
            (if (and (equal? inType 1)(equal? inFillAlpha TRUE))(set! base (car (gimp-image-merge-down image specular 1))))
        ;export image
            (file-png-save-defaults 1 image base outputFile outputFile)
        ;delete image
            (gimp-image-delete image)
        )
    )
)