(script-fu-register
    "script-fu-make-full-ctm-from-compact-ctm"
    "Make Full CTM from Compact CTM"
    "Takes an input folder of 5 compact ctm \
    tiles and creates 47 tiles needed for full\
    ctm"
    "Robert Rapier <xtrumpeterx@yahoo.com>"
    "MIT License"
    "October 13, 2019"
    ""

    SF-DIRNAME "Compact CTM Folder (files named 0.png - 4.png)" "%USERPROFILE%\\Pictures\\GIMP\\Maps" ;inInDir
    SF-DIRNAME "Output Folder" "%USERPROFILE%\\Pictures\\GIMP\\Output" ;inOutDir
    SF-ADJUSTMENT "First Output Tile #" '(0 0 9999 1 1 0 1) ;inFirst
    SF-OPTION "Type" '("Texture" "Specular Map" "Normal Map") ;inType
)
(script-fu-menu-register "script-fu-make-full-ctm-from-compact-ctm" "<Image>/Image")
(define (script-fu-make-full-ctm-from-compact-ctm inInDir inOutDir inFirst inType)
    (let*
        (
            (fileEnding "")
            (image (car (file-png-load 1 (string-append inInDir "\\" "0.png") (string-append inInDir "\\" "0.png"))))
            (res (car (gimp-image-width image)))
            (prehalf (- (/ res 2) 1))
            (half (/ res 2))
            (tile0 (car (gimp-image-get-active-layer image)))

            (tile1 (car (gimp-file-load-layer 1 image (string-append inInDir "\\" "1.png"))))
            (tile2 (car (gimp-file-load-layer 1 image (string-append inInDir "\\" "2.png"))))
            (tile3 (car (gimp-file-load-layer 1 image (string-append inInDir "\\" "3.png"))))
            (tile4 (car (gimp-file-load-layer 1 image (string-append inInDir "\\" "4.png"))))

            (workTile (car (gimp-layer-new image res res 1 "workTile" 100 28)))
        ) 
    (if (equal? (car (gimp-drawable-is-rgb tile0)) 0) (gimp-image-convert-rgb image))
    (if (equal? inType 0)
        (set! fileEnding (string-append ".png"))
        (if (equal? inType 1)
            (set! fileEnding (string-append "_s.png"))
            (set! fileEnding (string-append "_n.png"))))
    ;prepare layers
    (gimp-item-set-name tile0 "0.png")
    (gimp-image-insert-layer image tile1 0 0)
    (gimp-image-insert-layer image tile2 0 0)
    (gimp-image-insert-layer image tile3 0 0)
    (gimp-image-insert-layer image tile4 0 0)

    (gimp-image-insert-layer image workTile 0 0)

    (gimp-layer-add-alpha tile0)
    (gimp-layer-add-alpha tile1)
    (gimp-layer-add-alpha tile2)
    (gimp-layer-add-alpha tile3)
    (gimp-layer-add-alpha tile4)

    ;piece full ctm tiles together
    
    (define (export-pieced-tile TR BR BL TL index)

    (gimp-image-remove-layer image workTile)
    (set! workTile (car (gimp-layer-new image res res 1 "workTile" 100 28)))
    (gimp-image-insert-layer image workTile 0 0)
    ;top right
    (gimp-image-select-rectangle image 2 half 0 half half)
    (gimp-edit-copy TR)
    (gimp-floating-sel-anchor (car (gimp-edit-paste workTile TRUE)))
    ;bottom right
    (gimp-image-select-rectangle image 2 half half half half)
    (gimp-edit-copy BR)
    (gimp-floating-sel-anchor (car (gimp-edit-paste workTile TRUE)))
    ;bottom left
    (gimp-image-select-rectangle image 2 0 half half half)
    (gimp-edit-copy BL)
    (gimp-floating-sel-anchor (car (gimp-edit-paste workTile TRUE)))
    ;top left
    (gimp-image-select-rectangle image 2 0 0 half half)
    (gimp-edit-copy TL)
    (gimp-floating-sel-anchor (car (gimp-edit-paste workTile TRUE)))

    (file-png-save-defaults 1 image workTile (string-append inOutDir "//" (number->string (+ index inFirst)) fileEnding) (string-append inOutDir "//" (number->string (+ index inFirst)) fileEnding))
    )

    (export-pieced-tile tile0 tile0 tile0 tile0 0)
    (export-pieced-tile tile3 tile3 tile0 tile0 1)
    (export-pieced-tile tile3 tile3 tile3 tile3 2)
    (export-pieced-tile tile0 tile0 tile3 tile3 3)
    (export-pieced-tile tile3 tile4 tile2 tile0 4)
    (export-pieced-tile tile0 tile2 tile4 tile3 5)
    (export-pieced-tile tile4 tile4 tile2 tile2 6)
    (export-pieced-tile tile3 tile4 tile4 tile3 7)
    (export-pieced-tile tile1 tile4 tile4 tile4 8)
    (export-pieced-tile tile4 tile1 tile4 tile4 9)
    (export-pieced-tile tile4 tile4 tile1 tile1 10)
    (export-pieced-tile tile1 tile4 tile4 tile1 11)
    (export-pieced-tile tile0 tile2 tile2 tile0 12)
    (export-pieced-tile tile3 tile1 tile2 tile0 13)
    (export-pieced-tile tile3 tile1 tile1 tile3 14)
    (export-pieced-tile tile0 tile2 tile1 tile3 15)
    (export-pieced-tile tile4 tile3 tile0 tile2 16)
    (export-pieced-tile tile2 tile0 tile3 tile4 17)
    (export-pieced-tile tile4 tile3 tile3 tile4 18)
    (export-pieced-tile tile2 tile2 tile4 tile4 19)
    (export-pieced-tile tile4 tile4 tile4 tile1 20)
    (export-pieced-tile tile4 tile4 tile1 tile4 21)
    (export-pieced-tile tile4 tile1 tile1 tile4 22)
    (export-pieced-tile tile1 tile1 tile4 tile4 23)
    (export-pieced-tile tile2 tile2 tile2 tile2 24)
    (export-pieced-tile tile1 tile1 tile2 tile2 25)
    (export-pieced-tile tile1 tile1 tile1 tile1 26)
    (export-pieced-tile tile2 tile2 tile1 tile1 27)
    (export-pieced-tile tile4 tile1 tile2 tile2 28)
    (export-pieced-tile tile3 tile4 tile1 tile3 29)
    (export-pieced-tile tile1 tile4 tile2 tile2 30)
    (export-pieced-tile tile3 tile1 tile4 tile3 31)
    (export-pieced-tile tile1 tile4 tile1 tile1 32)
    (export-pieced-tile tile1 tile1 tile4 tile1 33)
    (export-pieced-tile tile1 tile4 tile1 tile4 34)
    (export-pieced-tile tile4 tile1 tile4 tile1 35)
    (export-pieced-tile tile2 tile0 tile0 tile2 36)
    (export-pieced-tile tile1 tile3 tile0 tile2 37)
    (export-pieced-tile tile1 tile3 tile3 tile1 38)
    (export-pieced-tile tile2 tile0 tile3 tile1 39)
    (export-pieced-tile tile1 tile3 tile3 tile4 40)
    (export-pieced-tile tile2 tile2 tile4 tile1 41)
    (export-pieced-tile tile4 tile3 tile3 tile1 42)
    (export-pieced-tile tile2 tile2 tile1 tile4 43)
    (export-pieced-tile tile4 tile1 tile1 tile1 44)
    (export-pieced-tile tile1 tile1 tile1 tile4 45)
    (export-pieced-tile tile4 tile4 tile4 tile4 46)



    (gimp-display-new image)
    )
) 